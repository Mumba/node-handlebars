/**
 * Public exports.
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as Handlebars from 'handlebars';

import equality from './helpers/equality';
import formatDate from './helpers/formatDate';
import formatMoment from './helpers/formatMoment';
import markdown from './helpers/markdown';
import math from './helpers/math';
import sprintf from './helpers/sprintf';

// Add helpers to the Handlebars object.
formatDate(Handlebars);
formatMoment(Handlebars);
equality(Handlebars);
markdown(Handlebars);
math(Handlebars);
sprintf(Handlebars);

export * from './ObjectBuilder';
