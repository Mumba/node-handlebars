/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Handlebars = require('handlebars');

/**
 * Recursively clone an array.
 */
function cloneArray(schema: any, data: any, renderer: Function) {
	return schema.map((element: any) => {
		return clone(element, data, renderer);
	});
}

/**
 * Recursively clone an object.
 */
function cloneObject(schema: any, data: any, renderer: Function) {
	const result: any = {};

	Object.keys(schema)
		.forEach(function (key) {
			result[key] = clone(schema[key], data, renderer);
		});

	return result;
}

/**
 * Clone a scalar property or data substitution property.
 */
function cloneScalar(value:string | number, data: any, renderer: Function) {
	if (typeof value === 'string') {
		return renderer(value, data);
	}

	return value;
}

/**
 * Build an object from a template.
 */
function clone(schema: any, data: any, renderer: Function) {
	if (Array.isArray(schema)) {
		return cloneArray(schema, data, renderer);
	}
	else if (typeof schema === 'object' && schema !== null) {
		return cloneObject(schema, data, renderer);
	}
	else {
		return cloneScalar(schema, data, renderer);
	}
}

export class ObjectBuilder {
	constructor(private schema = {}) {
	}

	public build(data: any) {
		// TODO could split the renderer out again if needed.
		return clone(this.schema, data, (value: any, data: any) => Handlebars.compile(value)(data));
	}
}
