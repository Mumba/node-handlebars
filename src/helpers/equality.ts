/**
 * @copyright Mumba Pty Ltd 2017. All rights reserved.
 * @license   Apache-2.0
 */

function eq(v1: any, v2: any) {
	return v1 === v2;
}

function ne(v1: any, v2: any) {
	return v1 !== v2;
}

function lt(v1: any, v2: any) {
	return v1 < v2;
}

function lte(v1: any, v2: any) {
	return v1 <= v2;
}

function gt(v1: any, v2: any) {
	return v1 > v2;
}

function gte(v1: any, v2: any) {
	return v1 >= v2;
}

function and(v1: any, v2: any) {
	return v1 && v2;
}

function or(v1: any, v2: any) {
	return v1 || v2;
}

export default function (Handlebars: any) {
	Handlebars.registerHelper({
		eq,
		ne,
		lt,
		lte,
		gt,
		gte,
		and,
		or
	});
}
