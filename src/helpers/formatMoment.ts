/**
 * @copyright Mumba Pty Ltd 2017. All rights reserved.
 * @license   Apache-2.0
 */

/**
 * Handlebars helper to format a moment object.
 *
 * @param context
 * @param block
 * @returns {string}
 */
function formatMoment(context: any, block: any) {
	const format = block.hash.format || 'MMM DD, YYYY hh:mm:ss A';

	if (context && context.format) {
		return context.format(format);
	}
	else {
		return context;
	}
}

export default function (Handlebars: any) {
	Handlebars.registerHelper('formatMoment', formatMoment);
}
