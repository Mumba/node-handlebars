/**
 * @copyright Mumba Pty Ltd 2018. All rights reserved.
 * @license   Apache-2.0
 */

const marked = require('marked');

/**
 * Handlebars helper to format markdown text.
 */
function markdown(context: any, block: any): string {
	if (context) {
		return marked(context);
	}
	else {
		return context;
	}
}

export default function (Handlebars: any) {
	Handlebars.registerHelper('markdown', markdown);
}
