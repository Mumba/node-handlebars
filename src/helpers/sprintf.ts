/**
 * @copyright Mumba Pty Ltd 2017. All rights reserved.
 * @license   Apache-2.0
 */

const vsprintf = require('sprintf-js').vsprintf;

/**
 * Handlebars helper to format a moment object.
 *
 * @param context
 * @param block
 * @returns {*}
 */
function sprintf(format: string, ...args: any[]) {
	if (args.length < 1) {
		return;
	}

	// Not sure if we need to do anything with this.
	const block = args.pop();

	return args.length > 0 ? vsprintf(format, args) : format;
}

export default function (Handlebars: any) {
	Handlebars.registerHelper('sprintf', sprintf);
}
