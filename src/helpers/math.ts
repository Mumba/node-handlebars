/**
 * @copyright Mumba Pty Ltd 2017. All rights reserved.
 * @license   Apache-2.0
 * @see {@link http://www.factmonster.com/ipka/A0881931.html}
 */

/**
 * Handlebars helper to subtract two numbers.
 *
 * @param augend
 * @param addend
 * @returns {*}
 */
function add(augend: number, addend: number) {
	return (+augend) + (+addend);
}

/**
 * Handlebars helper to subtract two numbers.
 *
 * @param minuend
 * @param subtrahend
 * @returns {*}
 */
function subtract(minuend: number, subtrahend: number) {
	return (+minuend) - subtrahend;
}

export default function (Handlebars: any) {
	Handlebars.registerHelper({
		add,
		subtract
	});
}
