/**
 * @copyright Mumba Pty Ltd 2017. All rights reserved.
 * @license   Apache-2.0
 */

import * as moment from 'moment-timezone';

/**
 * Handlebars helper to format a moment object.
 *
 * @param context
 * @param block
 * @returns {string}
 */
function formatDate(context: any, block: any) {
	const format = block.hash.format || 'MMM DD, YYYY hh:mm:ss A';
	const tz = block.hash.tz;

	if (context) {
		if (tz) {
			return moment.tz(context, this[tz]).format(format);
		}
		else {
			return moment.parseZone(context).format(format);
		}
	}
	else {
		return context;
	}
}

export default function (Handlebars: any) {
	Handlebars.registerHelper('formatDate', formatDate);
}
