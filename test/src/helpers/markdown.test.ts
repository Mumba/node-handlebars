/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright Mumba Pty Ltd 2018. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
const Handlebars = require('handlebars');

import instance from '../../../src/helpers/markdown';

describe('helpers/markdown', () => {
	let template: Function;

	beforeEach(() => {
		instance(Handlebars);
		template = Handlebars.compile('Do not {{{markdown body}}}');
	});

	it('should format markdown text', () => {
		const result = template({
			body: '**Shout**'
		});

		assert.equal(result, 'Do not <p><strong>Shout</strong></p>\n');
	});
});
