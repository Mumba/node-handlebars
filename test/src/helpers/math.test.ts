/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright Mumba Pty Ltd 2016. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
const Handlebars = require('handlebars');
const ERR = 0.0000001;

import instance from '../../../src/helpers/math';

describe('helpers/math', function () {
	before(function () {
		instance(Handlebars);
	});

	it('should add a number to a variable', function () {
		const template = Handlebars.compile('{{add num -0.05}}');

		assert(template({ num: 3.1415 }) - 3.0915 < ERR);
		assert(template({ num: '3.1415' }) - 3.0915 < ERR);
	});

	it('should subtract a number from a variable', function () {
		const template = Handlebars.compile('{{subtract num 0.05}}');

		assert(template({ num: 3.1415 }) - 3.0915 < ERR);
		assert(template({ num: '3.1415' }) - 3.0915 < ERR);
	});
});
