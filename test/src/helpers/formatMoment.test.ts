/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright Mumba Pty Ltd 2016. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import * as moment from 'moment';
const Handlebars = require('handlebars');

import instance from '../../../src/helpers/formatMoment';

describe('helpers/formatMoment', () => {
	let template: Function;

	beforeEach(() => {
		instance(Handlebars);
		template = Handlebars.compile('It starts at {{formatMoment startsAt format="YYYY/MM/DD"}}');
	});

	it('should handle date formatting', () => {
		const result = template({
			startsAt: moment.parseZone('2016-02-29T00:00:00+08:00')
		});

		assert.equal(result, 'It starts at 2016/02/29');
	});

	it('should ignore a non-moment date', () => {
		const result = template({
			startsAt: '2016-02-29T00:00:00+08:00'
		});

		assert.equal(result, 'It starts at 2016-02-29T00:00:00+08:00');
	});

	it('should ignore an undefined value', () => {
		const result = template({});

		assert.equal(result, 'It starts at ');
	});
});
