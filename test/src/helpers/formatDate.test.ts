/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright Mumba Pty Ltd 2016. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
const Handlebars = require('handlebars');

import instance from '../../../src/helpers/formatDate';

describe('helpers/formatDate', () => {
	let template: Function;

	beforeEach(() => {
		instance(Handlebars);
		template = Handlebars.compile('It starts at {{formatDate startsAt format="YYYY/MM/DD HH:mm:ss Z"}}');
	});

	it('should format a string date', () => {
		const result = template({
			startsAt: '2016-02-29T08:00:00+08:00'
		});

		assert.equal(result, 'It starts at 2016/02/29 08:00:00 +08:00');
	});

	it('should format a date with a timezone property', () => {
		const template = Handlebars.compile('It starts at {{formatDate startsAt format="YYYY/MM/DD HH:mm:ss Z" tz="timezone"}}');
		const result = template({
			startsAt: '2016-06-06T08:00:00Z',
			timezone: 'Australia/Perth'
		});

		assert.equal(result, 'It starts at 2016/06/06 16:00:00 +08:00');
	});

	it('should ignore an undefined value', () => {
		const result = template({});

		assert.equal(result, 'It starts at ');
	});
});
