/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright Mumba Pty Ltd 2016. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';

const Handlebars = require('handlebars');

import instance from '../../../src/helpers/equality';

describe('helpers/equality', () => {
	beforeEach(() => {
		instance(Handlebars);
	});

	it('eq', () => {
		const template = Handlebars.compile('{{#if (eq foo "bar")}}true{{else}}false{{/if}}');

		assert.equal(template({ foo: 'bar' }), 'true');
		assert.equal(template({ foo: 'foo' }), 'false');
	});

	it('eq null', () => {
		const template = Handlebars.compile('{{#if (eq foo null)}}true{{else}}false{{/if}}');

		assert.equal(template({ foo: null }), 'true');
		assert.equal(template({ foo: 'not null' }), 'false');
	});

	it('ne', () => {
		const template = Handlebars.compile('{{#if (ne foo "bar")}}true{{else}}false{{/if}}');

		assert.equal(template({ foo: 'not bar' }), 'true');
		assert.equal(template({ foo: 'bar' }), 'false');
	});

	it('lt', () => {
		const template = Handlebars.compile('{{#if (lt foo 2)}}true{{else}}false{{/if}}');

		assert.equal(template({ foo: 1 }), 'true');
		assert.equal(template({ foo: 3 }), 'false');
	});

	it('lte', () => {
		const template = Handlebars.compile('{{#if (lte foo 2)}}true{{else}}false{{/if}}');

		assert.equal(template({ foo: 2 }), 'true');
		assert.equal(template({ foo: 3 }), 'false');
	});

	it('gt', () => {
		const template = Handlebars.compile('{{#if (gt foo 2)}}true{{else}}false{{/if}}');

		assert.equal(template({ foo: 3 }), 'true');
		assert.equal(template({ foo: 1 }), 'false');
	});

	it('gte', () => {
		const template = Handlebars.compile('{{#if (gte foo 2)}}true{{else}}false{{/if}}');

		assert.equal(template({ foo: 2 }), 'true');
		assert.equal(template({ foo: 1 }), 'false');
	});

	it('and', () => {
		const template = Handlebars.compile('{{#if (and (gt foo 1) (lt foo 3))}}true{{else}}false{{/if}}');

		assert.equal(template({ foo: 2 }), 'true');
		assert.equal(template({ foo: 1 }), 'false');
		assert.equal(template({ foo: 3 }), 'false');
	});

	it('or', () => {
		const template = Handlebars.compile('{{#if (or (eq foo false) (eq foo true))}}true{{else}}false{{/if}}');

		assert.equal(template({ foo: false }), 'true');
		assert.equal(template({ foo: true }), 'true');
		assert.equal(template({ foo: null }), 'false');
	});
});
