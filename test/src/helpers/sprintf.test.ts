/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright Mumba Pty Ltd 2016. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
const Handlebars = require('handlebars');

import instance from '../../../src/helpers/sprintf';

describe('helpers/sprintf', function () {
	before(function () {
		instance(Handlebars);
	});

	it('should return nothing with no arguments', function () {
		const template = Handlebars.compile('{{sprintf}}');

		assert.equal(template({ str: 'foo', num: 3.1415 }), '');
	});

	it('should return format if no variables supplied', function () {
		const template = Handlebars.compile('{{sprintf "%s" }}');

		assert.equal(template({ str: 'foo', num: 3.1415 }), '%s');
	});

	it('should support multiple arguments', function () {
		const template = Handlebars.compile('{{sprintf "%s %.2f" str num }}');

		assert.equal(template({ str: 'foo', num: 3.1415 }), 'foo 3.14');
	});

	it('should support nested variables arguments', function () {
		const template = Handlebars.compile('{{sprintf "%.2f" foo.num }}');

		assert.equal(template({ foo: { num: 3.1415 }}), '3.14');
	});
});
