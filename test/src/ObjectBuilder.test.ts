/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {ObjectBuilder} from '../../src/index';

describe('Chris21PasswordService unit tests', () => {
	it('should build an object', () => {
		const data = {
			value: 'foo',
			two: 2
		};
		const schema = {
			scalar: 'scalar',
			value: '{{value}}',
			array: [
				'one',
				'{{two}}'
			],
			object: {
				bar: '{{value}}'
			}
		};
		const instance = new ObjectBuilder(schema);

		const result = instance.build(data);

		assert.equal(result.scalar, schema.scalar, 'should be a scalar');
		assert.equal(result.value, data.value, 'should be a value');
		assert.equal(result.array[0], schema.array[0], 'should be an array scalar');
		assert.equal(result.array[1], data.two);
		assert.equal(result.object.bar, data.value);
	});
});
