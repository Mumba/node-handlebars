# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.2.3] 25 Oct 2017
- Fixed `markdown` helper.

## [0.2.2] 25 Oct 2017
- Added `markdown` helper.

## [0.2.1] 25 Oct 2017
- Added optional `tz` flag to `formatDate`. This is the key to a timezone field by which to adjust the date. 

## [0.2.0] 4 Sep 2017
- Converted renderer function to `ObjectBuilder`.

## [0.1.1] 9 Dec 2016
- Added `formatDate` helper.

## [0.1.0] 13 Sep 2016
- Initial release
