[![build status](https://gitlab.com/Mumba/node-handlebars/badges/master/build.svg)](https://gitlab.com/Mumba/node-handlebars/commits/master)
[![coverage report](https://gitlab.com/Mumba/node-handlebars/badges/master/coverage.svg)](https://gitlab.com/Mumba/node-handlebars/commits/master)
## Mumba Handlebars renderer

A [Handlebars service](http://handlebarsjs.com/) with several helpers.

## Examples

### Equality

```typescript
import {ObjectBuilder} from 'mumba-handlebars';

const source = {
	// equalities
	eq: '{{#if (eq foo "bar")}}true{{else}}false{{/if}}'
};
const data = {
	foo: 'bar'
}
const builder = new ObjectBuilder(source);
const result = builder.build(source, data);
// result.eq == 'true'
```

## License

Apache-2.0

* * *

See [working with microservices](https://gitlab.com/Mumba/mumba-docs/wikis/guides/working-with-microservices) for more information on how to test this package.

&copy; 2017 Mumba Pty Ltd. All rights reserved.
